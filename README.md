# LG AIB Intermediate course - Machine learning for manufacturing systems 2019

## requirements

```
pytorch
torchvision
numpy
requests
matplotlib
pandas
sklearn
```

## Instllation (on windows with anaconda)
If your computer is not the case, please ask to the instructor or T.As.

```
# if use GPU
conda install pytorch torchvision cudatoolkit=10.0 -c pytorch
# in the case of only CPU
conda install pytorch torchvision cpuonly -c pytorch 
conda install -c anaconda numpy
conda install -c anaconda requests
conda install -c anaconda pandas
conda install scikit-learn
```