import numpy as np


def xlogy(xs, ys):
    return xs * np.log(ys)


def sample_data(num_samples, x_bound=[1e-10, 10], y_bound=[1e-10, 1.0], fn=xlogy):
    xs = np.random.uniform(low=x_bound[0], high=x_bound[1], size=(num_samples, 1))
    ys = np.random.uniform(low=y_bound[0], high=y_bound[1], size=(num_samples, 1))
    z = fn(xs, ys)
    x = np.concatenate((xs, ys), axis=1)
    return x, z
