import torch


class MultiLayerPerceptron(torch.nn.Module):

    def __init__(self, input_dim, hidden_dim, output_dim):
        self.input_layer = torch.nn.Linear(input_dim, hidden_dim)
        self.hidden_layer_1 = torch.nn.Linear(hidden_dim, hidden_dim)
        self.hidden_layer_2 = torch.nn.Linear(hidden_dim, hidden_dim)
        self.output_layer = torch.nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        x = self.input_layer(x)
        x = self.hidden_layer_1(x)
        x = self.hidden_layer_2(x)
        x = self.output_layer(x)
        return x
