import torch
import numpy as np


class MAPELoss:

    def __call__(self, pred, target):
        return torch.abs((target - pred) / target).mean() * 100

    def __repr__(self):
        return 'MAPELoss()'


def gaussianLL(mu, std, y):
    log_exp_terms = torch.pow(y - mu, 2) / (2 * torch.pow(std, 2))
    log_scaler_terms = -0.5 * (torch.log(torch.tensor(2.0)) + torch.log(torch.tensor(np.pi)) + 2 * torch.log(std))
    LL = log_scaler_terms - log_exp_terms
    return LL
