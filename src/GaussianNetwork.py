import torch
from src.MultiLayerPerceptron import MultiLayerPerceptron


class GaussianLayer(torch.nn.Module):

    def __init__(self, input_dim):
        super().__init__()
        self.z_mu = torch.nn.Linear(input_dim, 1)
        self.z_sigma = torch.nn.Linear(input_dim, 1)

    def forward(self, x):
        mu = self.z_mu(x)
        std = self.z_sigma(x)
        std = torch.exp(std)
        return mu, std


class GaussianNetwork(torch.nn.Module):

    def __init__(self, input_dim, hidden_dim):
        super().__init__()
        self.mlp = MultiLayerPerceptron(input_dim, hidden_dim)
        self.gaussian_layer = GaussianLayer(hidden_dim)

    def forward(self, x):
        x = self.mlp.forward(x)
        mu, std = self.gaussian_layer.forward(x)
        return mu, std
