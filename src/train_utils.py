def train(model, x_tensor, y_tensor, num_epochs, loss_fn, eval_fn, optimizer, print_every=100):
    """ Very basic trainer. No mini batching
    :param model: (torch.nn.module) model to train
    :param x_tensor: (torch.tensor) input tensors
    :param y_tensor: (torch.tensor) target tensors
    :param num_epochs: (int) Number of epochs
    :param loss_fn: (torch.nn.losses or equivalent that supports backward) a loss metric for training
    :param eval_fn: (torch.nn.losses or equivalent that supports backward) a loss metric to validate model
    :param optimizer: (Instance of torch.nn.optimim) an optimizer to train model
    :param print_every: (int) Report trainig progess every 'print_every'
    :return: train loss, validation loss
    """

    train_losses = []
    valid_losses = []

    for epoch in range(num_epochs):
        model.train()

        y_pred = model(x_tensor)

        loss = loss_fn(y_pred, y_tensor)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        train_losses.append(loss.item())

        model.eval()

        y_pred = model(x_tensor)
        loss = eval_fn(y_pred, y_tensor)

        valid_losses.append(loss.item())

        if epoch % print_every == 0:
            print('epoch : {}, train loss : {:.4f}, valid loss : {:.4f}'.format(epoch,
                                                                                train_losses[-1],
                                                                                valid_losses[-1]))
    return train_losses, valid_losses
